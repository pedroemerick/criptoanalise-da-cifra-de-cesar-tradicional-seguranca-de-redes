/**
* @file	    cifra.cpp
* @brief	Arquivo com a função principal do programa, que cifra as mensagens com 
            a cifra de césar tradicional
* @author   Pedro Emerick (p.emerick@live.com)
* @since	08/08/2018
* @date	    14/08/2018
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <cstdlib>
using std::atoi;

#include <string>
using std::string;

#include <cstring>

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <vector>
using std::vector;

#include <algorithm>
using std::find;

/**
* @brief Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param nome_arq Nome do arquivo a ser lido
* @return Mensagem lida do arquivo
*/
string ler_msg (string nome_arq)
{
    ifstream arquivo (nome_arq);
    if (!arquivo)
    {
        cerr << "--> Erro ao ler mensagem !" << endl;
        exit (1);
    }

    string mensagem;
    while (!arquivo.eof()) //enquanto end of file for false continua
    {
        string str_temp;
        getline (arquivo, str_temp);
        mensagem += str_temp;
        mensagem += "\n";
    }

    arquivo.close();

    return mensagem;
}

/**
* @brief Função que grava a mensagem cifrada em um arquivo
* @param mensagem Mensagem cifrada a ser gravada no arquivo
* @param nome_arq Nome do arquivo a ser gravado
*/
void gravar_msg (string mensagem, string nome_arq) 
{
    ofstream arquivo (nome_arq);

    if (!arquivo)
    {
        cerr << "--> Erro ao gravar mensagem cifrada !" << endl;
        exit (1);
    }

    arquivo << mensagem << endl;
    arquivo.close ();
}

/**
* @brief Função que cifra uma mensagem de acordo com a senha passada
* @param mensagem Mensagem a ser cifrada
* @param chave Chave para cifrar
*/
string cifrar (string mensagem, int chave) 
{
    string msg_cifrada;
    vector <string> alfabeto = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    int tamanho_msg = mensagem.size();
    
    for (int ii = 0; ii < tamanho_msg; ii++) {

        if (!ispunct (mensagem[ii]) && !isspace (mensagem[ii]) && mensagem[ii] > 0)
        {
            unsigned char nova_letra = (mensagem[ii] + chave);

            if ((nova_letra > 90 && mensagem[ii] <= 90 && mensagem[ii] >= 65) || 
                (nova_letra > 122 && mensagem[ii] <= 122 && mensagem[ii] >= 97)) 
            {
                nova_letra -= 26;
            }

	        msg_cifrada += nova_letra;
        }
	    else {
            msg_cifrada += mensagem[ii];
        }
    }

    return msg_cifrada;
}

/**
* @brief Função principal do programa.
*/
int main (int argc, char *argv[])
{
    // Verifica se são passados os argumentos corretamente
    if (argc != 4) {
        cout << "--> Argumentos invalidos ! Use './prog chave texto_claro texto_cifrado'" << endl;
        exit(1);
    }

    for (int ii = 0; ii < (int) strlen(argv[1]); ii++) {
        if (argv[1][ii] < 48 || argv[1][ii] > 57) {
            cout << "--> Passe uma chave numérica de 1 a 26 para cifragem !" << endl;
            exit(1);
        }
    }

    int chave = atoi(argv[1]);

    if (chave < 1 || chave > 26) {
        cout << "--> Passe uma chave de 1 a 26 para cifragem !" << endl;
        exit(1);
    }

    string mensagem = ler_msg(argv[2]);

    string msg_cifrada = cifrar (mensagem, chave);

    gravar_msg (msg_cifrada, argv[3]);

    cout << "--> Mensagem cifrada com sucesso !" << endl;

    return 0;
}