/**
* @file	    decifra.cpp
* @brief	Arquivo com a função principal do programa, que decifra as mensagens com 
            com força bruta cifradas com a cifra de césar tradicional
* @author   Pedro Emerick (p.emerick@live.com)
* @since	08/08/2018
* @date	    14/08/2018
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <cstdlib>
using std::atoi;

#include <string>
using std::string;
using std::to_string;

#include <cstring>

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <vector>
using std::vector;

#include <algorithm>
using std::find;

/**
* @brief Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param nome_arq Nome do arquivo a ser lido
* @return Mensagem lida do arquivo
*/
string ler_msg (string nome_arq)
{
    ifstream arquivo (nome_arq);
    if (!arquivo)
    {
        cerr << "Erro ao ler mensagem !!!" << endl;
        exit (1);
    }

    string mensagem;
    while (!arquivo.eof()) //enquanto end of file for false continua
    {
        string str_temp;
        getline (arquivo, str_temp);
        mensagem += str_temp;
        mensagem += "\n";
    }

    arquivo.close();

    return mensagem;
}

/**
* @brief Função que decifra uma mensagem de acordo com a chave/senha
* @param mensagem Mensagem a ser cifrada
* @param chave Chave para cifrar a mensagem
*/
string decifrar (string mensagem, int chave) 
{
    string msg_decifrada;
    vector <string> alfabeto = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    int tamanho_msg = mensagem.size();
    
    for (int ii = 0; ii < tamanho_msg; ii++) {

	    if (!ispunct (mensagem[ii]) && !isspace (mensagem[ii]) && (unsigned) mensagem[ii] <= 122)
        {
            unsigned char nova_letra = (mensagem[ii] - chave) % 256;
            
            if (nova_letra < 65 && mensagem[ii] <= 90 && mensagem[ii] >= 65) {
                nova_letra += 26;
            } else if (nova_letra < 97 && mensagem[ii] <= 122 && mensagem[ii] >= 97) {
                nova_letra += 26;
            }

	        msg_decifrada += nova_letra;
        }
	    else {
            msg_decifrada += mensagem[ii];
        }
    }

    return msg_decifrada;
}

/**
* @brief Função que grava a mensagem decifrada em um arquivo
* @param mensagem Mensagem decifrada a ser gravada no arquivo
* @param nome_arq Nome do arquivo a ser gravado
*/
void gravar_msg (string mensagem, string nome_arq) 
{
    ofstream arquivo (nome_arq);

    if (!arquivo)
    {
        cerr << "Erro ao gravar mensagem cifrada !!!" << endl;
        exit (1);
    }

    arquivo << mensagem << endl;
    arquivo.close ();
}


/**
* @brief Função principal do programa.
*/
int main (int argc, char *argv[])
{
    // Verifica se são passados os argumentos corretamente
    if (argc != 2) {
        cout << "--> Argumentos invalidos ! Use './prog texto_cifrado'" << endl;
        exit(1);
    }

    string mensagem = ler_msg(argv[1]);

    // Tenta decifrar com todas as chaves possiveis
    for (int ii = 1; ii <= 26; ii++) 
    {
        string msg_decifrada = decifrar (mensagem, ii);

        string arquivo = "./data/chave";
        arquivo += to_string (ii);
        arquivo += ".txt";

        gravar_msg (msg_decifrada, arquivo);
    }

    cout << "--> Decifragem em força bruta realizada, verifique os arquivos './data/chave{1..26}.txt' para encontrar a mensagem que deseja !" << endl;

    return 0;
}
