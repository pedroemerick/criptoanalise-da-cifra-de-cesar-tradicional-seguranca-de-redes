## CRIPTOANÁLISE DA CIFRA DE CÉSAR TRADICIONAL

Este repositório contém dois programas que permitem cifrar e decifrar (com força bruta) um arquivo texto seguindo a [Cifra de César Tradicional](https://pt.wikipedia.org/wiki/Cifra_de_C%C3%A9sar).

---

### Considerações Gerais:

Os programas que permitem cifrar e decifrar (com força bruta) utilizam o alfabeto de letras minúsculas e maiúsculas, quaisquer outros caracteres que não sejam deste alfabeto usado, serão ignorados, tanto na cifragem quanto na decifragem.

Sobre os comandos que o Makefile permite:

* "make clean" = apaga os arquivos objeto e binário;
* "make" = compila os dois programas;
* "make cifra" = compila o programa que permite cifrar;
* "make decifra" = compila o programa que permite decifrar;
* "make doc" = gera a documentação dos programas, em que será possivel visualizar acesssando a pasta doc e abrindo o arquivo index.html.

---

### Para cifrar:
* Compile o programa que permite cifrar, usando o comando "make cifra";
* Execute passando os parâmetros corretos:  "./bin/cifra chave mensagem.txt mensagem.sec".
    * chave = a chave escolhida para cifragem (lembre-se a chave só será permitida de 1 a 26)
    * mensagem.txt = arquivo que contém a mensagem a ser cifrada
    * mensagem.sec = arquivo em que a mensagem cifrada deve ser armazenada

---

### Para decifrar (com força bruta):
* Compile o programa que permite decifrar, usando o comando "make decifra";
* Execute passando os parâmetros corretos:  "./bin/decifra mensagem.sec";
    * mensagem.sec = arquivo que contém a mensagem cifrada
* O programa irá criar 26 arquivos na pasta "data", nomeados de "chave{1..26}.txt", em que foi tentado decifrar a mensagem com todas as chaves possíveis, verifique os arquivos e veja se encontra a mensagem desejada.


